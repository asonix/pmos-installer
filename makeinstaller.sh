#!/bin/bash

THIS_SCRIPT=$0

log() {
	echo ">> $1"
}

# $1: The var to replace
# $2: The new value
# $3: The file to run on
# e.g. set_template_var UIFLAVOUR Phosh /etc/hostname
set_template_var() {
	sed "s/%$1%/$2/g" -i "$3"
}

die() {
	[[ -n $1 ]] && log "$1"
	exit 1
}

if ! which pmbootstrap > /dev/null 2> /dev/null; then
	log "pmbootstrap not found, can't guess values"
	NO_PMB=1
fi

TEMPDIR="$(mktemp)"
BASEDIR="$(dirname "$0")"
RAMDISK_DEFAULT="$TEMPDIR/initramfs.cpio.gz"
UIFLAVOUR="flavour"
SCRIPT="$TEMPDIR/META-INF/com/google/android/update-binary"

# Get variables from pmbootstrap
if [ -z "$NO_PMB" ]; then
	PMBWORK="$(pmbootstrap config work)"
	PMBDEVICE="$(pmbootstrap config device)"
	KERNEL="$(echo "$PMBWORK"/chroot_rootfs_"$PMBDEVICE"/boot/vmlinuz-dtb)"
	VERSION="$(grep "VERSION_ID=" < "$PMBWORK"/chroot_rootfs_"$PMBDEVICE"/etc/os-release | cut -d"=" -f2 | cut -d"\"" -f2)"
	OS_NAME="postmarketOS"
	DEVICEINFO_PATH="$PMBWORK/cache_git/pmaports/device/community/device-$PMBDEVICE/deviceinfo"
	UIFLAVOUR="$(pmbootstrap config ui)"
	DEVICE="$(echo "$PMBDEVICE" | cut -d"-" -f2)"
fi

usage() {
	echo "$THIS_SCRIPT: A mainline Linux OS flashable installer generator"
	echo "Primarily for postmarketOS, this installer can be used to generate a flashable zip"
	echo "for any linux distro, provided a pmos formatted deviceinfo file, an ext4 rootfs image and a kernel."
	echo ""
	echo "Run 'pmbootstrap install --split && pmbootstrap export' to build pmOS, then"
	echo "$THIS_SCRIPT [ -p PART ] [ -r RAMDISK] [ -k KERNEL ] [ -c BOOT_IMAGE] [ -i ROOTFS_IMAGE ] [ -s SLOT ] [ -a ] "
	echo "[ -u UIFLAVOUR ] [ -d DEVICE ] [ -o OS_NAME ] [ -v VERSION ] [ -e DEVICEINFO_PATH ]"
	echo ""
	echo "-p <PART>                where PART is the userdata partition (e.g. 'sda17')"
	echo "-r <RAMDISK>             specify a prebuilt ramdisk"
	echo "-k <KERNEL>              specify a custom kernel path (required when no pmbootstrap or chose BOOT_IMAGE)"
	echo "-c <BOOT_IMAGE>          specify a custom boot.img path (required when no pmbootstrap or chose KERNEL)"
	echo "-i <ROOTFS_IMAGE>        specify a custom rootfs path (required when no pmbootstrap)"
	echo "-s <SLOT>                specify the slot to install to, A-only devices MUST use -a (below) [ Default = a ]"
	echo "-u <UIFLAVOUR>           set UI flavour, e.g. 'phosh', 'lomiri' (required when no pmbootstrap)"
	echo "-d <DEVICE>              set device codename (checked during install) (required when no pmbootstrap)"
	echo "-o <OS_NAME>             set name of OS that will be installed"
	echo "-v <VERSION>             version number of the OS being installed [ Default = YY.MM ]"
	echo "-e <DEVICEINFO_PATH>     path to deviceinfo file used to generate boot image (required when no pmbootstrap or chose BOOT_IMAGE)"
	echo "-g <ROOTFS_SIZE>         size to expand the rootfs image to after installing in GB [ Default = 16 ]"
	echo "-a                       build for A-Only devices (non A/B compatible)"
	echo "-b <OUTFILE>             output zip file path"
	exit 1
}

while getopts "p:r:k:c:i:s:u:d:o:v:e:g:b:ah" options; do
	case "${options}" in
		p)
			PART=${OPTARG}
			;;
		r)
			RAMDISK=${OPTARG}
			;;
		k)
			KERNEL=${OPTARG}
			;;
	  c)
			BOOT=${OPTARG}
			;;
		i)
			ROOTFS=${OPTARG}
			;;
		s)
			SLOT=${OPTARG}
			;;
		u)
			UIFLAVOUR=${OPTARG}
			;;
		d)
			DEVICE=${OPTARG}
			;;
		o)
			OS_NAME=${OPTARG}
			;;
		v)
			VERSION=${OPTARG}
			;;
		e)
			DEVICEINFO_PATH=${OPTARG}
			;;
		g)
			ROOTFS_SIZE=${OPTARG}
			;;
		b)
			OUTFILE=${OPTARG}
			;;
		a)
			A_ONLY=1
			;;
		h)
			usage
			;;
		:)
			die "-${OPTARG} requires an argument"
			;;
		*)
			usage
			;;
	esac
done

# Check for dependencies
if [ -z "$BOOT" ]; then
  [[ $(which mkbootimg) ]] || die "mkbootimg not found, get it from https://android.googlesource.com/platform/system/tools/mkbootimg/ and symlink it as 'mkbootimg' somewhere in your PATH"
fi

hash cpio || die "Can't find cpio binary, is cpio installed?"
hash gzip || die "Can't find gzip binary, is gzip installed?"

if [ -n "$KERNEL" ] && [ -z "$DEVICEINFO_PATH" ]; then
	die "deviceinfo file not set, -e is compulsory when not using pmbootstrap"
fi

if [ -n "$DEVICEINFO_PATH" ]; then
  # shellcheck source=/dev/null
  source "$DEVICEINFO_PATH"
fi

if [ -z "$PART" ] && [ -z "$RAMDISK" ]; then
	# shellcheck disable=SC2154
	PART="$deviceinfo_rootfs_part"
fi

if [ -z "$PART" ] && [ -z "$RAMDISK" ]; then
	die "-p option is missing, please see https://gitlab.com/sdm845-mainline/pmtools#pmos-installer for details"
fi

if [ -z "$NO_PMB" ]; then
	if [ -z "$ROOTFS" ]; then
		ROOTFS="$(readlink /tmp/postmarketOS-export/"$PMBDEVICE"-root.img)"
	fi
	if [ -z "$ROOTFS" ]; then
		die "Can't find rootfs image, make sure you run pmbootstrap install with --split, or specify it on the cmdline"
	fi
else
	if [ -z "$ROOTFS" ]; then
		die "Not using pmbootstrap, -i is compulsory"
	fi
	if [ -z "$DEVICE" ]; then
		die "Not using pmbootstrap, -d is compulsory"
	fi
  if [ -z "$KERNEL" ] && [ -z "$BOOT" ]; then
		die "Not using pmbootstrap, -k or -c is compulsory"
	fi
	if [ -n "$KERNEL" ] && [ -n "$BOOT" ]; then
		die "Not using pmbootstrap, chose either -k or -c"
	fi
fi

log "OS Version is: '$VERSION'"
log "UI flavour is: '$UIFLAVOUR'"
log "Working directory is: '$TEMPDIR'"
log "Device is: '$DEVICE'"
if [ -n "$KERNEL" ]; then
  log "Using kernel: '$KERNEL'"
else
  log "Using boot.img: '$BOOT'"
fi
echo

# Clean work dir
[[ -n $TEMPDIR ]] && [[ -f $TEMPDIR ]] && rm -r "$TEMPDIR"

mkdir -p "$TEMPDIR"

cp -r META-INF "$TEMPDIR"/

set_template_var "VERSION" "$VERSION" "$SCRIPT"
set_template_var "UIFLAVOUR" "$UIFLAVOUR" "$SCRIPT"
set_template_var "DEVICE" "$DEVICE" "$SCRIPT"
set_template_var "OS_NAME" "$OS_NAME" "$SCRIPT"

# User options >>>
ROOTFS_SIZE=${ROOTFS_SIZE:-16}
set_template_var "SIZE" "$ROOTFS_SIZE" "$SCRIPT"

# Only enters loop if SLOT is not set by cmdline (and not building for an A-only Treble device)
if [ -z "$SLOT" ] && [ -z "$A_ONLY" ]; then
	SLOT="a"

	log "Will install to slot $SLOT, modify the zip file name to change target slot"
	log "e.g. \"postmarketOS-20200806-enchilada-phosh-SLOT_a.zip\""
fi

# << User options

log "Copying exported rootfs to zip directory"
cp "$ROOTFS" "$TEMPDIR/$OS_NAME.img"

if [ -n "$KERNEL" ]; then
  if [ -z "$RAMDISK" ]; then
    RAMDISK=$RAMDISK_DEFAULT
    "$BASEDIR"/initrd/build.sh "$RAMDISK"
  fi

  log "Using kernel image: $KERNEL"
  log "Using ramdisk: $RAMDISK"

  set -x

  # shellcheck disable=SC2154
  mkbootimg \
  --base "$deviceinfo_flash_offset_base" \
  --kernel_offset "$deviceinfo_flash_offset_kernel" \
  --ramdisk_offset "$deviceinfo_flash_offset_ramdisk" \
  --second_offset "$deviceinfo_flash_offset_second" \
  --tags_offset "$deviceinfo_flash_offset_tags" \
  --pagesize "$deviceinfo_flash_pagesize" \
  --kernel "$KERNEL" \
  --ramdisk "$RAMDISK" \
  --cmdline "rootfs_part=$PART rootfs_path=.stowaways/$OS_NAME.img" \
  -o "$TEMPDIR/${OS_NAME}_boot.img"
  set +x
else
  cp "$BOOT" "$TEMPDIR/${OS_NAME}_boot.img"
fi

if [ -z "$OUTFILE" ]; then
	if [ -z "$A_ONLY" ]; then
		OUTFILE="$PWD/$OS_NAME-$(date +"%Y%m%d")-$DEVICE-$UIFLAVOUR-SLOT_$SLOT.zip"
	else
		OUTFILE="$PWD/$OS_NAME-$(date +"%Y%m%d")-$DEVICE-$UIFLAVOUR.zip"
	fi
fi

log "Creating flashable zip: $OUTFILE"
echo

pushd "$TEMPDIR" > /dev/null || exit
	time zip -x initramfs.cpio.gz -r "$OUTFILE" . || die "Couldn't create flashable zip!"
popd > /dev/null || exit

rm -r "$TEMPDIR"

echo
log "Flashable zip at $OUTFILE"
echo 
echo -e "\e[32m===================\e[0m"
echo -e "\e[32m    ALL DONE!\e[0m"
echo -e "\e[32m===================\e[0m"
